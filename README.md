# chartjs-adapter-dayjs

## Overview

This adapter allows the use of day.js with Chart.js time scale.

Requires Chart.js **2.8.0** or later and dayjs **1.9.6** or later.

**NOTE:** This adapter was designed for Chart.js v3 (which requires a separate date adapter for time scales to work properly), using this adapter in any version prior to 3 will override the default date-adapter

## Installation

### npm

``` bash
npm install dayjs chartjs-adapter-dayjs --save
```

```javascript
import Chart from 'chart.js';
import 'chartjs-adapter-dayjs';
```

### CDN

By default, `https://cdn.jsdelivr.net/npm/chartjs-adapter-dayjs` returns the latest (minified) version, however it's [highly recommended](https://www.jsdelivr.com/features) to always specify a version in order to avoid breaking changes. This can be achieved by appending `@{version}` to the URL:

```html
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3"></script>
<script src="https://cdn.jsdelivr.net/npm/dayjs@1.96.0"></script>
<script src="https://cdn.jsdelivr.net/npm/chartjs-adapter-dayjs@1.0.0"></script>
```

Read more about jsDelivr versioning on their [website](http://www.jsdelivr.com/).

## Configuration

### v2 [Chart.js documention](https://www.chartjs.org/docs/latest)

Read the Chart.js documention [v2](https://www.chartjs.org/docs/latest) | [v3](https://www.chartjs.org/docs/master) for possible date/time related options. For example, the time scale `time.*` options [v2](https://www.chartjs.org/docs/latest/axes/cartesian/time.html#configuration-options) | [v3](https://www.chartjs.org/docs/master/axes/cartesian/time.html#configuration-options) can be overridden using the [Day.js formats](https://day.js.org/docs/en/display/format).

## Development

You first need to install node dependencies (requires [Node.js](https://nodejs.org/)):

``` bash
> npm install
```

## License

`chartjs-adapter-dayjs` is available under the [MIT license](LICENSE.md).

## Credits

-----------------------------------------------------------------------------
Moment Adapter source && README template

[Ben McCann](https://github.com/benmccann)

[Evert Timberg](https://github.com/etimberg)

[stockiNail](https://github.com/stockiNail)

-----------------------------------------------------------------------------
