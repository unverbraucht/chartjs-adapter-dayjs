import Chart from "chart.js";

import dayjs from "dayjs";

//Needed to handle the custom parsing
import CustomParseFormat from "dayjs/plugin/customParseFormat";

//Needed to handle quarter format
import AdvancedFormat from "dayjs/plugin/advancedFormat";

//Needed to handle adding/subtracting quarter
import QuarterOfYear from "dayjs/plugin/quarterOfYear";

//Needed to handle localization format
import LocalizedFormat from "dayjs/plugin/localizedFormat";

import isoWeek from "dayjs/plugin/isoWeek";

dayjs.extend(AdvancedFormat);

dayjs.extend(QuarterOfYear);

dayjs.extend(LocalizedFormat);

dayjs.extend(CustomParseFormat);

dayjs.extend(isoWeek);

const FORMATS = {
  datetime: "MMM D, YYYY, h:mm:ss a",
  millisecond: "h:mm:ss.SSS a",
  second: "h:mm:ss a",
  minute: "h:mm a",
  hour: "hA",
  day: "MMM D",
  week: "ll",
  month: "MMM YYYY",
  quarter: "[Q]Q - YYYY",
  year: "YYYY",
};

Chart._adapters._date.override({
  _id: "dayjs", //DEBUG,
  formats: () => FORMATS,
  parse: (value, format) => {
    const valueType = typeof value;

    if (value === null || valueType === "undefined") {
      return null;
    }

    let parsedValue = null;

    if (valueType === "string" && typeof format === "string") {
      parsedValue = dayjs(value, format);
    } else if (!(value instanceof dayjs)) {
      parsedValue = dayjs(value);
    }

    return parsedValue.isValid() ? parsedValue.valueOf() : null;
  },
  format: (time, format) => dayjs(time).format(format),
  add: (time, amount, unit) => dayjs(time).add(amount, unit).valueOf(),
  diff: (max, min, unit) => dayjs(max).diff(dayjs(min), unit),
  startOf: (time, unit, weekday) => {
    if (unit === "isoWeek") {
      // Ensure that weekday has a valid format
      //const formattedWeekday
      return dayjs(time).isoWeekday(weekday).startOf("day").valueOf();
    }

    return dayjs(time).startOf(unit).valueOf();
  },
  endOf: (time, unit) => dayjs(time).endOf(unit).valueOf(),
});

export default Chart;